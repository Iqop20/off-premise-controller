package inconnu.offprem;

/**
 * This is a data structure to hold cloud provider connection information
 */
public class CloudEntry {
    public int id;
    public String cloud_provider;
    public String identity;
    public String credential;
    public String container;
    public String ip_address;
    public String port;
    public String path;

    public CloudEntry(int id, String cloud_provider, String identity, String credential, String container, String ip_address, String port) {
        this.id  = id;
        this.cloud_provider = cloud_provider;
        this.identity = identity;
        this.credential = credential;
        this.container = container;
        this. ip_address = ip_address;
        this.port = port;
        this.path="";
    }

    public CloudEntry(int id, String cloud_provider, String identity, String credential, String container, String ip_address, String port,String path) {
        this.id  = id;
        this.cloud_provider = cloud_provider;
        this.identity = identity;
        this.credential = credential;
        this.container = container;
        this. ip_address = ip_address;
        this.port = port;
        this.path=path;
    }



    public CloudEntry(int id, String cloud_provider, String identity, String credential, String container) {
        this.id  = id;
        this.cloud_provider = cloud_provider;
        this.identity = identity;
        this.credential = credential;
        this.container = container;
        this. ip_address = null;
        this.port = null;
    }
}
