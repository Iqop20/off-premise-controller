package inconnu.offprem;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;

/**
 * This class implements a runnable that when called attempts to upload the data from the previous day
 * The upload process is based on ARGUS
 */
public class PeriodicCacheUploader implements Runnable{
    @Override
    public void run() {

        /*
            Get the previous day file
         */
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        if (day==1){
            if (month==Calendar.JANUARY){
                year--;
            }else month--;
        }

        /*
            Get the file of the previous day
         */
        File toUpload = new File(Configs.storagePath+"/"+day+"."+month+"."+year);

        /*
            Upload file and Store metadata information
         */
        if (toUpload.exists()) {

            try {

                byte[] HMAC_key = CryptoFunctions.generateHMACKey();
                byte[] AES_key = CryptoFunctions.generateRandomAESKey();

                //Upload file and get chunk encryption metadata
                ArrayList<Document> store =  processUpload(toUpload,HMAC_key,AES_key);

                MongoClient mongoClient = MongoClients.create(Configs.mongoDB);
                MongoCollection<Document> collection = mongoClient.getDatabase("off-prem").getCollection("clouds");
                Document file = new Document("_id", toUpload.getName())
                        .append("filename", toUpload.getName())
                        .append("hmac_key", Base64.getEncoder().encodeToString(HMAC_key))
                        .append("aes_key",Base64.getEncoder().encodeToString(AES_key))
                        .append("chunks", store);

                System.out.println("Stored: " + toUpload.getName());
                collection.insertOne(file);
                mongoClient.close();

                toUpload.delete();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            System.out.println("No data for day "+toUpload.getName());
        }
    }


    /** This method performs the upload operation. It is based on the ARGUS upload process
     * @param toUpload The file to upload
     * @param HMAC_key The hmac key for that file
     * @param aes_key The aes key for that file
     * @return Returns the hmac and iv for each processed chunk
     */
    ArrayList<Document> processUpload(File toUpload,byte[] HMAC_key,byte[] aes_key){

        ArrayList<Document> toReturn = new ArrayList<>();

        try{

            Thread[] threads = new Thread[Configs.NUMBER_UPLOAD_THREADS];


            FileInputStream fileInputStream = new FileInputStream(toUpload);

            byte[] block = Utils.readBlock(Configs.blockSize, fileInputStream);
            int i=0;
            assert block!=null;
            while(block.length!=0){
                String hmac = CryptoFunctions.calculateHMAC(block, HMAC_key);
                byte[] iv = CryptoFunctions.generateRandomAESIV();

                Document entry = new Document();
                entry.append("hmac",hmac);
                entry.append("iv",Base64.getEncoder().encodeToString(iv));
                entry.append("part_number",i);

                toReturn.add(entry);

                byte[] encrypted = CryptoFunctions.encryptAES(block, aes_key, iv);

                Upload.Uploader uploader = new Upload.Uploader(toUpload.getName(), encrypted, i,Configs.cloudEntries);

                boolean found=false;
                while(!found) {
                    for(int j=0;j<Configs.NUMBER_UPLOAD_THREADS && !found;j++) {
                        if (threads[j]==null || !threads[j].isAlive()) {
                            threads[j]=new Thread(uploader);
                            threads[j].start();
                            found=true;
                        }
                    }
                }

                i++;
            }

            for(i=0;i<Configs.NUMBER_UPLOAD_THREADS;i++) {
                if (threads[i] != null) {
                    threads[i].join();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return toReturn;
    }
}
