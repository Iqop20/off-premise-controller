package inconnu.offprem;


public class Configs {
    public static String orionEndpoint=""; //"http://orion:1026/v2";
    public static boolean https=false;
    public static String mongoDB = "";
    public static int port = 8000;
    public static String ipAddress="";
    public static String storagePath="";
    public static int blockSize = 20*1024*1024;
    public static CloudEntry[] cloudEntries;


    /*
        Reed Solomon Settings
     */
    public static final int REED_SOLOMON_DATA_PARTS = 2;
    public static final int REED_SOLOMON_PARITY_PARTS = 1;



    //Symmetric Key Cryptography
    public static final String SYMMETRIC_KEY_ALGO_SETTING = "AES/CTR/NoPadding";
    public static final String SYMMETRIC_KEY_ALGO = "AES";
    public static final String SYMMETRIC_KEY_FACTORY = "PBKDF2WithHmacSHA256";
    public static final int SYMMETRIC_KEY_SIZE = 256;


    //HMAC
    public static final String HMAC_ALGO = "HMACSHA256";


    /*
        ARGUS settings
     */
    public static int NUMBER_UPLOAD_THREADS = 10;
    public static int NUMBER_DOWNLOAD_THREADS = 10;
    public static String PRIVATE_CLOUD_PROVIDER = "openstack-swift";
}
