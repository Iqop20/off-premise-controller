package inconnu.offprem.endpoints;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import inconnu.offprem.*;
import org.apache.commons.io.IOUtils;
import org.bson.Document;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.io.Payload;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.concurrent.*;

import static inconnu.offprem.Configs.NUMBER_DOWNLOAD_THREADS;

/**
 * This class provides the methods that are called when a download operation is issued
 * The download operation on this implementation downloads data from a day of data collection
 * The metadata information about the day file is stored on the mongo database available at the URL stored on Configs.mongoDB
 * The download process is based on ARGUS
 */
@RestController
public class Downloader {

    @RequestMapping(value="/download/{day}",method = RequestMethod.GET, produces= MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void downloader(@PathVariable String day, HttpServletResponse response) throws IOException {

        ServletOutputStream outputStream = response.getOutputStream();


        //GET METADATA FROM MONGO
        MongoClient mongoClient = MongoClients.create(Configs.mongoDB);
        MongoCollection<Document> collection = mongoClient.getDatabase("off-prem").getCollection("clouds");

        Document fileInfo = collection.find(new Document("_id", day)).first();

        mongoClient.close();
        if (fileInfo!=null){
            byte[] hmac_key = Base64.getDecoder().decode(fileInfo.getString("hmac_key"));
            byte[] aes_key = Base64.getDecoder().decode(fileInfo.getString("aes_key"));
            ArrayList<Document> f = (ArrayList<Document>) fileInfo.get("chunks");



            //Start the download process
            Future[] download_future = new Future[NUMBER_DOWNLOAD_THREADS];


            ExecutorService pool = Executors.newFixedThreadPool(NUMBER_DOWNLOAD_THREADS);

            int number_of_parts = f.size();

            for (int i = 0; i < Math.min(NUMBER_DOWNLOAD_THREADS, number_of_parts); i++) {
                download_future[i] = pool.submit(new DownloadCallable(fileInfo.getString("filename"),hmac_key,aes_key,f.get(i), i));

            }

            /*
                Using threads at least one file part is downloaded ahead of time improving performance
             */
            for(int part_number=0;part_number<number_of_parts;part_number++) {
                while(!download_future[part_number%NUMBER_DOWNLOAD_THREADS].isDone());

              /*
                  Sending the content
               */
                byte[] content = new byte[0];
                try {
                    content = (byte[]) download_future[part_number%NUMBER_DOWNLOAD_THREADS].get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    response.setStatus(500);
                }
                System.out.println("Sending "+ part_number);
                outputStream.write(content);
                outputStream.flush();
                System.out.println("Done "+part_number);

                //Starting the download of the next file part
                if (part_number+NUMBER_DOWNLOAD_THREADS < number_of_parts) {
                    download_future[part_number % NUMBER_DOWNLOAD_THREADS] = pool.submit(new DownloadCallable(fileInfo.getString("filename"),hmac_key,aes_key,f.get(part_number), part_number + NUMBER_DOWNLOAD_THREADS));
                }

            }

            outputStream.flush();
            outputStream.close();

        }
    }


    class DownloadCallable implements Callable<byte[]>{
        String filename;
        byte[] hmac_key;
        byte[] aes_key;
        Document chunk;
        int part_number;

        public DownloadCallable(String filename,byte[] hmac_key,byte[] aes_key,Document chunk,int part_number) {
            this.filename =filename;
            this.hmac_key = hmac_key;
            this.aes_key = aes_key;
            this.chunk = chunk;
            this.part_number = part_number;
        }

        @Override
        public byte[] call() throws Exception {
            byte[][] chunks = new byte[Configs.cloudEntries.length][];
            int i = 0;
            for (CloudEntry cloudEntry : Configs.cloudEntries) {
                BlobStore currentBlobStore;
                if (cloudEntry.cloud_provider.equals(Configs.PRIVATE_CLOUD_PROVIDER)) {
                    currentBlobStore = CloudConnectionFunctions.getOpenStackBlobStore(cloudEntry.ip_address, cloudEntry.port, cloudEntry.identity, cloudEntry.credential, cloudEntry.path);
                } else {
                    currentBlobStore = CloudConnectionFunctions.getPublicCloudBlobStore(cloudEntry.cloud_provider, cloudEntry.identity, cloudEntry.credential);
                }
                chunks[i] = doDownloadToTheBlobStore(currentBlobStore, cloudEntry.container, part_number + "." + filename + "." + i);
                i++;
            }

            byte[] encrypted = ReedSolomonFunctions.decode_erasure(chunks);

            byte[] decrypted = CryptoFunctions.decryptAES(encrypted, aes_key, Base64.getDecoder().decode(chunk.getString("iv")));

            String calc_HMAC = CryptoFunctions.calculateHMAC(decrypted, hmac_key);

            if (calc_HMAC.equals(chunk.getString("hmac"))) {
                return decrypted;
            } else System.out.println("HMAC failed");

            return null;
        }


        public byte[] doDownloadToTheBlobStore(BlobStore blobStore, String container_name, String stored_name) throws IOException {

            Blob blob = blobStore.getBlob(container_name, stored_name);

            Payload payload = blob.getPayload();
            final InputStream inputStream = payload.openStream();
            return IOUtils.toByteArray(inputStream);
        }

    }

}
