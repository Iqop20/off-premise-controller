package inconnu.offprem.endpoints;

import inconnu.offprem.Utils;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class contains methods to receive the subscription notifications
 * When a subscription update is received it is stored locally and at the end of each day the data of the previous day is uploaded (this is executed by PeriodicCacheUploader)
 */
@RestController
public class NotificationReceiver {

    @RequestMapping(value="/notify",method = RequestMethod.POST)
    public void notificationReceiver(@RequestBody String input){
        JSONObject jsonObject = new JSONObject(input);
        Utils.storeLocationDataIntoFile(jsonObject);
    }
}
