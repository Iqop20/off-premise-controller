package inconnu.offprem;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import static inconnu.offprem.Configs.*;

/**
 * This class contains helper methods for cryptographic operations
 */
public class CryptoFunctions {


    /*
        HMAC - Authentication
     */
    public static byte[] generateHMACKey() throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(HMAC_ALGO);
        SecretKey secretKey = keyGenerator.generateKey();

        return secretKey.getEncoded();
    }

    public static String calculateHMAC(byte[] file_content, byte[] key) throws NoSuchAlgorithmException, InvalidKeyException {

        SecretKey secretKey = new SecretKeySpec(key, HMAC_ALGO);

        Mac mac = Mac.getInstance(HMAC_ALGO);
        mac.init(secretKey);

        return DatatypeConverter.printHexBinary(mac.doFinal(file_content)).toUpperCase();

    }

  /*
      Symmetric key crypto
   */


    public static byte[] generateRandomAESKey() throws Exception {

        /*
            Generate Randomness
         */
        char [] randomKey;
        byte [] randomSalt;


        SecureRandom secureRandom= new  SecureRandom();
        byte[] temp = new byte[SYMMETRIC_KEY_SIZE /8];
        secureRandom.nextBytes(temp);

        randomKey = new String(temp).toCharArray();

        randomSalt = new byte[SYMMETRIC_KEY_SIZE /8];
        secureRandom.nextBytes(randomSalt);



        /*
            Generate Key
         */

        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(SYMMETRIC_KEY_FACTORY);
        PBEKeySpec spec = new PBEKeySpec(randomKey,randomSalt,5, SYMMETRIC_KEY_SIZE);


        return secretKeyFactory.generateSecret(spec).getEncoded();
    }


    public static byte[] generateRandomAESIV() throws Exception {
        SecureRandom ivSecureRandom = SecureRandom.getInstance("SHA1PRNG");
        byte[] iv = new byte[16];
        ivSecureRandom.nextBytes(iv);
        return iv;
    }




    public static byte[] encryptAES(byte[] toEncrypt, byte[] key,byte[] iv) throws Exception {

        Cipher cipher = Cipher.getInstance(SYMMETRIC_KEY_ALGO_SETTING);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, SYMMETRIC_KEY_ALGO);
        cipher.init(Cipher.ENCRYPT_MODE,secretKeySpec,ivParameterSpec);
        return cipher.doFinal(toEncrypt);

    }

    public static byte[] decryptAES(byte[] toDecrypt, byte[] key,byte[] iv) throws Exception {

        Cipher cipher = Cipher.getInstance(SYMMETRIC_KEY_ALGO_SETTING);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, SYMMETRIC_KEY_ALGO);
        cipher.init(Cipher.DECRYPT_MODE,secretKeySpec,ivParameterSpec);
        return cipher.doFinal(toDecrypt);
    }
}

