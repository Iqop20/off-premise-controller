package inconnu.offprem;

import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.blobstore.options.PutOptions;



public class Upload {
    /**Effective upload of a chunk shard to a cloud provider
     * @param blobStore The blobstore of the cloud provider
     * @param container_name The container name of that provider
     * @param stored_name The name to be stored on the cloud provider
     * @param content The byte[] containing the content
     * @return A boolean stating if the process completed or failed
     */
    public static boolean doUploadToTheBlobStore(BlobStore blobStore, String container_name, String stored_name , byte[] content) {
        Blob blob = blobStore.blobBuilder("argus_store/"+stored_name)
                .payload(content)
                .contentLength(content.length)
                .build();

        try {
            blobStore.putBlob(container_name, blob, PutOptions.Builder.multipart());
            System.out.println("[*]Success uploading "+stored_name+" to the cloud");
            return true;
        }catch (Exception e){
            System.out.println("[*]Failed uploading "+stored_name+" to the cloud due to "+e);
            return false;
        }
    }


    /**
     * This runnable allows to parallelize the upload of data to the louds
     */
    public static class Uploader implements Runnable{

        byte[] block;
        int block_number;
        CloudEntry[] clouds;
        String stored_name;

        public Uploader(String stored_name,byte[] block, int block_number, CloudEntry[] clouds) {
            super();
            this.block = block;
            this.block_number = block_number;
            this.clouds = clouds;
            this.stored_name = stored_name;
        }

        @Override
        public void run() {

            /*
                Do encoding and for each resulting shard upload it to a different cloud
             */

            byte[][] erasure_coded_block = ReedSolomonFunctions.encode_erasure(block);

            int i=0;
            for(CloudEntry cloudEntry : clouds) {
                BlobStore currentBlobStore;
                if (cloudEntry.cloud_provider.equals(Configs.PRIVATE_CLOUD_PROVIDER)){
                    currentBlobStore = CloudConnectionFunctions.getOpenStackBlobStore(cloudEntry.ip_address, cloudEntry.port, cloudEntry.identity, cloudEntry.credential,cloudEntry.path);
                }else{
                    currentBlobStore = CloudConnectionFunctions.getPublicCloudBlobStore(cloudEntry.cloud_provider, cloudEntry.identity, cloudEntry.credential);
                }
                //Retry Uploading
                while(!doUploadToTheBlobStore(currentBlobStore, cloudEntry.container, block_number+"."+stored_name+"."+i,erasure_coded_block[0])){
                    System.out.println("[*] Retrying upload due to error");
                }
                i++;
            }
        }
    }

}
