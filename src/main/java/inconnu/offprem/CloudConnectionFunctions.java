package inconnu.offprem;

import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.openstack.keystone.config.KeystoneProperties;
import java.util.Properties;

/**
 * This class presents helper functions that enables to connect to all the jClouds supported cloud providers
 * If using public cloud the getPublicCloudBlobStore() should be used, otherwise getOpenStackBlobStore()
 */
public class CloudConnectionFunctions {

    public static BlobStore getPublicCloudBlobStore(String cloud_provider, String identity, String credential){
        return ContextBuilder.newBuilder(cloud_provider).credentials(identity, credential).buildView(BlobStoreContext.class).getBlobStore();
    }

    public static BlobStore getOpenStackBlobStore(String ip_address, String port, String identity, String credential,String path){
        Properties overrides = new Properties();

        overrides.put(KeystoneProperties.CREDENTIAL_TYPE,"tempAuthCredentials");


        return ContextBuilder.newBuilder("openstack-swift")
                .endpoint("http://"+ip_address+":"+port+path+"/auth/v1.0")
                .credentials(identity, credential)
                .overrides(overrides)
                .buildApi(BlobStoreContext.class).getBlobStore();
    }

    public static void releaseBlobStore(BlobStore blobStore){
        blobStore.getContext().close();
    }



}
