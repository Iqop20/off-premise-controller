package inconnu.offprem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;

public class Utils {
    /**This method decodes de information obtained from the received json and stores it on the cache file in csv format
     * @param jsonObject The received update in json
     */
    public static void storeLocationDataIntoFile(JSONObject jsonObject) {
        JSONArray dataArray = jsonObject.getJSONArray("data");

        for (Object j: dataArray){
            JSONObject js = (JSONObject) j;

            String type = js.getString("type");

            StringBuilder title_csv = new StringBuilder();
            StringBuilder contents_csv = new StringBuilder();
            for (Iterator<String> it = js.keys(); it.hasNext(); ) {
                String k = it.next();
                title_csv.append(k).append(", ");
                if (!k.equals("type") && !k.equals("id")) {

                    switch (js.getJSONObject(k).getString("type")){
                        case "double":
                        case "Double":
                        case "decimal":
                        case "Decimal":
                            contents_csv.append(js.getJSONObject(k).getDouble("value")).append(", ");
                            break;
                        case "long":
                        case "Long":
                        case "Number":
                        case "number":
                        case "Integer":
                        case "integer":
                            contents_csv.append(js.getJSONObject(k).getLong("value")).append(", ");
                            break;
                        case "string":
                        case "String":
                        case "Text":
                        case "text":
                            contents_csv.append(js.getJSONObject(k).getString("value")).append(", ");
                            break;
                        case "Array":
                        case "array":
                            contents_csv.append("'").append(js.getJSONObject(k).getJSONArray("value").toString()).append("'").append(", ");
                            break;
                        case "geo:json":
                            contents_csv.append("'").append(js.getJSONObject(k).getJSONObject("value").toString()).append("'").append(", ");
                            break;
                    }

                }else{
                    contents_csv.append(js.getString(k)).append(", ");
                }
            }


            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH);
            int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

            File f = new File(Configs.storagePath+"/"+day+"."+month+"."+year);

            if (!f.exists()) {
                try {
                    f.createNewFile();
                    FileWriter fr = new FileWriter(f);
                    fr.write(title_csv +"\n");
                    fr.flush();
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                FileWriter fr = new FileWriter(f, true);
                fr.append(String.valueOf(contents_csv)).append("\n");
                fr.flush();
                fr.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /** This method reads a block with a specific size from the input stream. If the stream does not have the required size the amount of data available is returned
     * @param block_size The block size
     * @param is The content input stream
     * @return The byte[] with content extracted
     * @throws Exception If an error occurs an exception is thrown
     */
    public static byte[] readBlock(int block_size, InputStream is) throws Exception {
        int total_amount_read =0;
        int buffer_size=1024;

        byte[] block = new byte[block_size];

        boolean beginning=true;

        while(total_amount_read!=block_size){
            byte[] buffer;
            if (total_amount_read+buffer_size>block_size){
                buffer = new byte[block_size-total_amount_read];

            }else{
                buffer = new byte[buffer_size];
            }
            int read = is.read(buffer);
            if (read==-1){
                if (beginning){
                    return  null;
                }else break;
            }

            System.arraycopy(buffer,0,block,total_amount_read,read);
            beginning=false;
            total_amount_read+=read;
        }

        return Arrays.copyOfRange(block,0,total_amount_read);
    }
}
