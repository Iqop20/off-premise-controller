package inconnu.offprem;

import com.backblaze.ReedSolomon.ReedSolomon;
import static inconnu.offprem.Configs.*;
import java.nio.ByteBuffer;
import java.util.Arrays;


/**
 * Set of helper functions to deal with Reed Solomon encoding and decoding
 */
public class ReedSolomonFunctions {


    static final int TOTAL_SHARDS = REED_SOLOMON_DATA_PARTS + REED_SOLOMON_PARITY_PARTS;
    static final int BYTES_IN_INT = 4;

    public static byte[][] encode_erasure(byte[] file_content){

        final int fileSize = file_content.length;

        // Figure out how big each shard will be.  The total size stored
        // will be the file size (8 bytes) plus the file.
        final int storedSize = fileSize + BYTES_IN_INT;
        final int shardSize = (storedSize + REED_SOLOMON_DATA_PARTS - 1) / REED_SOLOMON_DATA_PARTS;

        // Create a buffer holding the file size, followed by
        // the contents of the file.
        final int bufferSize = shardSize * REED_SOLOMON_DATA_PARTS;
        final byte [] allBytes = new byte[bufferSize];

        ByteBuffer.wrap(allBytes).putInt(fileSize);

        System.arraycopy(file_content,0,allBytes,BYTES_IN_INT,file_content.length);


        // Make the buffers to hold the shards.
        byte [] [] shards = new byte [TOTAL_SHARDS] [shardSize];

        // Fill in the data shards
        for (int i = 0; i < REED_SOLOMON_DATA_PARTS; i++) {
            System.arraycopy(allBytes, i * shardSize, shards[i], 0, shardSize);
        }

        // Use Reed-Solomon to calculate the parity.
        ReedSolomon reedSolomon = new ReedSolomon(REED_SOLOMON_DATA_PARTS, REED_SOLOMON_PARITY_PARTS);
        reedSolomon.encodeParity(shards, 0, shardSize);



        return shards;
    }
    public static byte[] decode_erasure(byte[][] partial_files) throws Exception {
        final byte [] [] shards = new byte [TOTAL_SHARDS] [];
        final boolean [] shardPresent = new boolean [TOTAL_SHARDS];
        int shardSize = 0;
        int shardCount = 0;
        for (int i = 0; i < TOTAL_SHARDS; i++) {
            if (partial_files[i]!=null) {
                shardSize = partial_files[i].length;
                shards[i] = new byte [shardSize];
                shardPresent[i] = true;
                shardCount += 1;
                System.arraycopy(partial_files[i],0,shards[i],0,shardSize);
            }
        }

        // We need at least DATA_SHARDS to be able to reconstruct the file.
        if (shardCount < REED_SOLOMON_DATA_PARTS) {
            throw new Exception("Not enough shards present");
        }

        // Make empty buffers for the missing shards.
        for (int i = 0; i < TOTAL_SHARDS; i++) {
            if (!shardPresent[i]) {
                shards[i] = new byte [shardSize];
            }
        }

        // Use Reed-Solomon to fill in the missing shards
        ReedSolomon reedSolomon = new ReedSolomon(REED_SOLOMON_DATA_PARTS, REED_SOLOMON_PARITY_PARTS);
        reedSolomon.decodeMissing(shards, shardPresent, 0, shardSize);

        // Combine the data shards into one buffer for convenience.
        // (This is not efficient, but it is convenient.)
        byte [] allBytes = new byte [shardSize * REED_SOLOMON_DATA_PARTS];
        for (int i = 0; i < REED_SOLOMON_DATA_PARTS; i++) {
            System.arraycopy(shards[i], 0, allBytes, shardSize * i, shardSize);
        }

        // Extract the file length


        int size = ByteBuffer.wrap(allBytes).getInt();


        allBytes = Arrays.copyOfRange(allBytes, BYTES_IN_INT,size+BYTES_IN_INT);


        // Write the decoded file

        return allBytes;
    }
}
