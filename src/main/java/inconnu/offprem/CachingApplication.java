package inconnu.offprem;

import com.squareup.okhttp.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * This instantiates the notification receiver server and requests a subscription to receive all the updates from all the entities on the platform
 * When the server is about to be destroyed a request to cancel the subscription is executed before destroying the server
 * This class requires the following arguments:
 * <ul>
 *     <li><b>Orion URL: </b>The URL where an instance of FIWARE Orion is reading</li>
 *     <li><b>MongoDB URL: </b>The MongoDB URL</li>
 *     <li><b>Local Cache Storage Path: </b>A local path where the cached will be stored</li>
 *     <li><b>Cloud config file: </b>The cloud config file containing the credentials and other information related to the access to cloud providers</li>
 *     <li><b>Notification Address: </b>The address of the notification</li>
 * </ul>
 */
@SpringBootApplication
@Configuration
@PropertySource("classpath:application.properties")
public class CachingApplication {

	static String subscriptionID;
	static ScheduledFuture<?> scheduledFuture;

	public static void main(String[] args) throws Exception {
		Configs.orionEndpoint = args[0];
		Configs.mongoDB = args[1];
		Configs.storagePath = args[2];
		Configs.cloudEntries = processCloudEntries(args[3]);

		if (args.length>4)  Configs.ipAddress = args[4];


		/*
			Generating subscription request
		 */
		JSONObject patternToReceive = new JSONObject();
		patternToReceive.put("idPattern",".*");


		JSONArray jsonArray = new JSONArray();
		jsonArray.put(0,patternToReceive);


		System.out.println("Notification listening endpoint: "+((Configs.https)?("https://"):("http://"))+Configs.ipAddress+":"+Configs.port+"/notify");

		JSONObject notificationHttpInfo = new JSONObject();
		notificationHttpInfo.put("url",((Configs.https)?("https://"):("http://"))+Configs.ipAddress+":"+Configs.port+"/notify");


		JSONObject notificationInfo = new JSONObject();
		notificationInfo.put("http",notificationHttpInfo);


		JSONObject body = new JSONObject();
		body.put("subject",new JSONObject().put("entities",jsonArray));
		body.put("notification",notificationInfo);


		RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),body.toString());

		Request request = new Request.Builder()
				.url(Configs.orionEndpoint+"/subscriptions")
				.post(requestBody)
				.build();

		OkHttpClient okHttpClient = new OkHttpClient();

		/*
			Checking subscription response
		 */
		try {
			Response response = okHttpClient.newCall(request).execute();
			if (response.code()>=200 && response.code()<=300) {
				subscriptionID=response.headers("Location").get(0).split("/v2/subscriptions/")[1];
				/*
					Start the periodic cache uploader with 1 cache uploader per day
				 */
				scheduledFuture = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new PeriodicCacheUploader(), 0, 1, TimeUnit.DAYS);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		//RUN application
		SpringApplication.run(CachingApplication.class, new String[]{});
	}

	/**This method reads the clouds config file and returns an array of the cloud providers on the file
	 * @param configFile The clouds config file
	 * @return An array containing the cloud provider entries found
	 * @throws FileNotFoundException If the config file does not exist an error is thrown
	 */
	private static CloudEntry[] processCloudEntries(String configFile) throws FileNotFoundException {
		ArrayList<CloudEntry> toRet = new ArrayList<>();
		Scanner scanner = new Scanner(new File(configFile));
		int i=0;
		while(scanner.hasNext()){
			String entry = scanner.nextLine();
			if (!entry.startsWith("#")){
				String[] split = entry.split(",");
				String provider = split[0];
				String user = split[1];
				String credential = split[2];
				String container = split[3];

				if (provider.equals(Configs.PRIVATE_CLOUD_PROVIDER)){
					toRet.add(new CloudEntry(i,provider,user,credential,container,split[4],split[5]));
				}else toRet.add(new CloudEntry(i,provider,user,credential,container));

				i++;
			}
		}
		return toRet.toArray(new CloudEntry[]{});
	}

	/**
	 * This method is executed before the complete destruction of the server and allows to cancel the subscription made on the main function
	 */
	@PreDestroy
	public void destroy(){
		System.out.println("Removing uploader thread");
		scheduledFuture.cancel(false);
		System.out.println("Removing subscription");

		OkHttpClient okHttpClient = new OkHttpClient();

		Request request = new Request.Builder()
				.url(Configs.orionEndpoint+"/subscriptions/"+subscriptionID)
				.delete()
				.build();

		try {
			Response response = okHttpClient.newCall(request).execute();

			System.out.println("Subscription removed: "+response.code());
			response.body().close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
