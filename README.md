# Off-Premise Controller
**Description:** Component on the project architecture that enables caching and cloud based persistency 

**Project:** Inconnu

**Position on FIWARE architecture:** Persistency Enabler

**Code Documentation Format:** javadoc

***

## Contents
* Architecture
* Compilation
* Execution

### Architecture
The figure bellow represent an overview of the entire architecture and where this component is placed.
![Inconnu architecture overview](readme_imgs/all.png "Inconnu Architecture Overview")

In more detail the operation of downloading/upload is described by the figure bellow.
![Off-premise Controller Upload and Download Overview](readme_imgs/arch.png "Off-premise Controller Upload and Download Overview")

For more details check the implementation and architecture sections on the dissertation
### Compilation
This component is built in java with maven as a dependency manager and automatic compilation tool, to compile this component make sure to have the following installed:
* openjdk-11-jdk
* maven

To compile execute ```mvn clean package``` on the repository root folder. The compilation process will generate a **jar** file located at ```./target/server-0.1.jar```
### Execution
To execute run the following:
```shell
java -jar ./target/server-0.1.jar <OrionURL> <mongoDBURL> <clouds.cfg path> <addressToReceiveNotifications>
```
```clouds.cfg``` is a file where the cloud providers to be used are configured, it is required that 3 cloud providers to be configured in order for the system to work. In this configuration file all the lines started with # are ignored.

Bellow we show an example of a ```clouds.cfg``` configuration
```shell
#FORMAT: provider,user,credential,container,ip?,port?
# ip and port are only required if using openstack-swift jclouds provider
openstack-swift,name,credential,container,ip,port
azure-blob,name,credential,container
google-cloud-storage,name,credential,container
#aws-s3,name,credential,container
```
The provider names follows the jclouds specification, for more information check the jClouds documentation (https://jclouds.apache.org/reference/providers/)
***
For more information about the inner workings of this component check the **javadoc** documentation